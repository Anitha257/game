import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Vector;

/**
 * Created by HP on 5/23/2017.
 */
public class UIManager {

    private JFrame mainframe;
    private JPanel hp;
    private JPanel cp;
    private JPanel sp;
    private JPanel wp;
    private JLabel declaration;

    JButton buttons[][] = new JButton[3][3];

    Vector<String> diagonal1 = new Vector<>();
    Vector<String> diagonal2 = new Vector<>();

    private int count = 0;
    int player1count = 0;
    int player2count = 0;

    public UIManager() {

        prepareUI();
    }

    public static void main(String[] args) {

        UIManager UI = new UIManager();
    }

    public void prepareUI() {

        mainframe = new JFrame();
        mainframe.setSize(500, 500);

        wp = new JPanel(new BorderLayout());

        hp = new JPanel();
        //hp.setPreferredSize(new Dimension(20,20));

        cp = new JPanel(new GridLayout(3, 3));
        sp = new JPanel(new BorderLayout());

        JPanel westrpanel = new JPanel();
        westrpanel.setPreferredSize(new Dimension(50,50));
        wp.add(westrpanel,BorderLayout.WEST);
        JPanel bottomPanel = new JPanel();
        declaration = new JLabel();

        diagonal1.add("00");
        diagonal1.add("11");
        diagonal1.add("22");

        diagonal2.add("02");
        diagonal2.add("11");
        diagonal2.add("20");


        JLabel Player1 = new JLabel("Player 1 ");
        JLabel b1 = new JLabel();
        JLabel player2 = new JLabel(" player 2 ");
        JLabel b2 = new JLabel();


        JButton reset = new JButton("reset");

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {

                buttons[i][j] = new JButton();
                buttons[i][j].setActionCommand(String.valueOf(i) + String.valueOf(j));
                buttons[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        if (count == 0 || count % 2 == 0) {

                            ((JButton) e.getSource()).setText("+");
                            ((JButton) e.getSource()).setBackground(Color.pink);

                            ((JButton) e.getSource()).setEnabled(false);
                            //System.out.println(((JButton) e.getSource()).getActionCommand());
                        } else {

                            ((JButton) e.getSource()).setText("0");
                            ((JButton) e.getSource()).setBackground(Color.white);

                            ((JButton) e.getSource()).setEnabled(false);
                        }

                        count++;


                        if (check4win(((JButton) e.getSource()).getActionCommand(), ((JButton) e.getSource()).getText())) {
                            //JOptionPane.showMessageDialog(null, "Game Over");
                            if (((JButton) e.getSource()).getText().equals("+")){

                                declaration.setText("Player 1 winner                          \n" +
                                        "Player 2 looser");
                                player1count++;
                                b1.setText(String.valueOf(player1count));


                                BorderLayout layout = (BorderLayout) wp.getLayout();
                                wp.remove(layout.getLayoutComponent(BorderLayout.CENTER));
                                wp.add(declaration, BorderLayout.CENTER);


                            }else if (((JButton) e.getSource()).getText().equals("0")){

                                declaration.setText("Player 2 winner                           \n" +
                                        "Player 1 looser");
                                player2count++;
                                b2.setText(String.valueOf(player2count));

                                BorderLayout layout = (BorderLayout) wp.getLayout();
                                wp.remove(layout.getLayoutComponent(BorderLayout.CENTER));
                                wp.add(declaration, BorderLayout.CENTER);

                            }
                        }else if (count == 9) {


                            declaration.setText("Draw Match");

                            BorderLayout layout = (BorderLayout) wp.getLayout();
                            wp.remove(layout.getLayoutComponent(BorderLayout.CENTER));
                            wp.add(declaration, BorderLayout.CENTER);

                        }
                    }
                });

                cp.add(buttons[i][j]);
            }
        }

        //hp.add(new JLabel("TIC IAC TOE"));
        Font font = new Font("Tic Tac Toe",Font.BOLD,24);
        JLabel l = new JLabel("Tic TAc Toe");
        l.setFont(font);
        hp.add(l);

        hp.setPreferredSize(new Dimension(50,50));
        wp.add(hp, BorderLayout.NORTH);

        cp.setPreferredSize(new Dimension(80,80));

        cp.setBackground(Color.black);wp.add(cp, BorderLayout.CENTER);


        JPanel innerPanel = new JPanel(new GridLayout(2,2));
        innerPanel.setPreferredSize(new Dimension(120,20));

        sp.add(innerPanel);

        bottomPanel.add(reset);
        //wp.add(bottomPanel,BorderLayout.SOUTH);
        wp.add(sp, BorderLayout.EAST);

        mainframe.add(wp);
        mainframe.setVisible(true);
    }

    public boolean check4win(String actionCommand, String text) {

        System.out.println(actionCommand);

        if (checkDiagonal(actionCommand,text)){

            return true;
        }else if (checkRow(Integer.valueOf("" + actionCommand.charAt(0)), text)) {

            return true;
        }else if (checkColumn(Integer.valueOf("" + actionCommand.charAt(1)), text)) {

            return true;
        } else return false;
    }

    public boolean checkRow(int row, String text) {

        int count = 0, j = 0;

        while (j < 3) {

            if (buttons[row][j].getText().equals(text)) {
                count++;
            }
            j++;
        }

        if (count == 3) {
            return true;
        } else return false;
    }

    public boolean checkColumn(int column, String text) {

        int count = 0, i = 0;

        while (i < 3) {

            if (buttons[i][column].getText().equals(text)) {
                count++;
            }
            i++;
        }

        if (count == 3) {
            return true;
        } else return false;
    }

    public boolean checkDiagonal(String diagonal, String text) {

        if (diagonal1.contains(diagonal)) {

            if (buttons[1][1].getText().equals(text) && buttons[0][0].getText().equals(text) && buttons[2][2].getText().equals(text)) {

                return true;
            }else return false;
        } else if (diagonal2.contains(diagonal)) {

            if (buttons[1][1].getText().equals(text) && buttons[0][2].getText().equals(text) && buttons[2][0].getText().equals(text)) {

                return true;
            }else return false;
        } else return false;
    }
}
